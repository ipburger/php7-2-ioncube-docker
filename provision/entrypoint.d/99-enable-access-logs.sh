# Enable access logs if DEBUG env var is defined
if [ ! -z ${DEBUG+x} ]; then
  # /etc/httpd
  sed -i 's/^[[:blank:]]*CustomLog/#&/' '/opt/docker/etc/httpd/vhost.common.d/02-no-logs.conf'
  sed -i 's/CustomLog \/dev\/null/CustomLog \/docker.stdout/g' '/opt/docker/etc/httpd/conf.d/20-log.conf'

  # /etc/php
  sed -i '/;access.log/s/^;//g' '/opt/docker/etc/php/fpm/pool.d/application.conf'
  sed -i '/;access.log/s/^;//g' '/opt/docker/etc/php/fpm/pool.d/docker.conf'
fi
